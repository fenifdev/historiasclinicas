<?php

namespace historiasclinicas;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'patients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dni', 'name', 'birthday', 'user_id'];

    public function stories()
    {
      return $this->hasMany('historiasclinicas\Story');
    }

    public function user()
    {
      return $this->belongsTo('historiasclinicas\User');
    }


}
