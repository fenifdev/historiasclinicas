<?php

namespace historiasclinicas\Http\Requests;

use historiasclinicas\Http\Requests\Request;

class PatientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
            {
                  return [
                    'name' => 'required|min:3|max:100',
                    'dni' => 'required|unique:patients|integer',
                    'birthday' => 'required|date_format:Y-m-d',
                    'user_id' => 'required|exists:users,id'
                  ];
            }
            case 'PUT':
            {
                  return [
                      'name' => 'required|min:3|max:100',
                      'dni' => 'required|unique:patients,dni,'.$this->patient.'|integer',
                      'birthday' => 'required|date_format:Y-m-d',
                      'user_id' => 'required|exists:users,id'
                  ];
            }
        }
    }
}
