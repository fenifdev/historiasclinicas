<?php

namespace historiasclinicas\Http\Requests;

use historiasclinicas\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      switch ($this->method())
      {
          case 'POST':
          {
                return [
                  'name' => 'required|min:6|max:100',
                  'email' => 'required|unique:users,email',
                  'role' => 'required',
                  'password' => 'required|min:6|max:100'
                ];
          }
          case 'PUT':
          {
                return [
                    'name' => 'required|min:3|max:100',
                    'email' => 'required|unique:users,email,'.$this->user,
                    'role' => 'required'
                ];
          }
      }
    }
}
