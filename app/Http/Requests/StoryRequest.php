<?php

namespace historiasclinicas\Http\Requests;

use historiasclinicas\Http\Requests\Request;

class StoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
            {
                return [
                  'story' => 'required|min:3|max:100',
                  'patient_id' => 'required|exists:patients,id'
                ];
            }
            case 'PUT':
            {
                return [
                  'story' => 'required|min:3|max:100'
                ];
            }
        }
    }
}
