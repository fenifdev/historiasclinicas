<?php

namespace historiasclinicas\Http\Controllers;

use Illuminate\Http\Request;

use historiasclinicas\Http\Requests;
use historiasclinicas\Http\Requests\PatientRequest;
use historiasclinicas\Http\Controllers\Controller;

use \historiasclinicas\Patient;
use \historiasclinicas\User;
use Carbon\Carbon;
use Auth;
use Yajra\Datatables\Datatables;
use Form;

class PatientController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
      Carbon::setLocale('es');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->role=='admin'){
        $patients = Patient::paginate(10);
      }else{
        $patients = Patient::where('user_id',Auth::user()->id)->paginate(10);
      }

      return view('patient.index',compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role=='admin'){
            $users = User::All();
            $data_users = User::lists('name','id');
        }else{
            $data_users = [Auth::user()->id => Auth::user()->name];
        }

        return view('patient.create',compact('data_users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatientRequest $request)
    {
        if(Auth::user()->role=='doctor'){
            $request['user_id'] = Auth::user()->id;
        }
        Patient::create($request->all());
        return redirect('/patient')->with('status', 'Patiente Store!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->role=='admin'){
          $patient = Patient::findOrFail($id);
        }else{
          $patient = Patient::where('id',$id)
                            ->where('user_id', Auth::user()->id)
                            ->firstOrFail();
        }

        return view('patient.show')->with(['patient' => $patient]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->role=='admin'){
        $patient = Patient::findOrFail($id);
        $data_users = User::lists('name','id');
      }else{
        $patient = Patient::where('id',$id)
                          ->where('user_id', Auth::user()->id)
                          ->firstOrFail();
        $data_users = [Auth::user()->id => Auth::user()->name];
      }

      return view('patient.edit')->with(['patient' => $patient,'data_users' => $data_users]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, $id)
    {
        if(Auth::user()->role=='admin'){
            $patient = Patient::findOrFail($id);
        }else{
            $request['user_id'] = Auth::user()->id;
            $patient = Patient::where('id',$id)
                              ->where('user_id', Auth::user()->id)
                              ->firstOrFail();
        }

        $patient->fill($request->all());
        $patient->save();
        return redirect('/patient')->with('status', 'Patiente Save!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role=='admin'){
            $patient = Patient::findOrFail($id);
        }else{
            $patient = Patient::where('id',$id)
                              ->where('user_id', Auth::user()->id)
                              ->firstOrFail();
        }
        $patient->delete();
        return redirect('/patient')->with('status', 'Patiente Delete!');
    }

    public function createStory($id){
      if(Auth::user()->role=='admin'){
        $patient = Patient::findOrFail($id);
      }else{
        $patient = Patient::where('id',$id)
                          ->where('user_id', Auth::user()->id)
                          ->firstOrFail();
      }
      return view('patient.createStory',['patient_id'=>$patient->id]);
    }

    public function datatablesData()
    {
        if(Auth::user()->role=='admin'){
          #return Datatables::of(Patient::query())->make(true);
          return Datatables::of(Patient::query())->addColumn('action', function ($patient) {
                      $algo = link_to_route('patient.edit', $title = 'Editar', $parameters = $patient->id, $attributes = ['class'=>'btn btn-primary']);
                      $algo .= link_to_route('patient.show', $title = 'Historia', $parameters = $patient->id, $attributes = ['class'=>'btn btn-info']);
                      $algo .= Form::open(['route' => ['patient.destroy',$patient->id] ,'method'=>'DELETE', 'style'=>'display:inline']);
                      $algo .= Form::submit('Eliminar',['class'=>'btn btn-danger']);
                      $algo .= Form::close();
                      return $algo;
                  })
                  ->make(true);
        }else{
          $patients = Patient::where('user_id',Auth::user()->id);
          #return Datatables::of($patients)->make(true);
          return Datatables::of($patients)->addColumn('action', function ($patient) {
                      $algo = link_to_route('patient.edit', $title = 'Editar', $parameters = $patient->id, $attributes = ['class'=>'btn btn-primary']);
                      $algo .= link_to_route('patient.show', $title = 'Historia', $parameters = $patient->id, $attributes = ['class'=>'btn btn-info']);
                      $algo .= Form::open(['route' => ['patient.destroy',$patient->id] ,'method'=>'DELETE', 'style'=>'display:inline']);
                      $algo .= Form::submit('Eliminar',['class'=>'btn btn-danger']);
                      $algo .= Form::close();
                      return $algo;
                  })
                  ->make(true);
        }
    }
}
