<?php

namespace historiasclinicas\Http\Controllers;

use Illuminate\Http\Request;

use historiasclinicas\Http\Requests;
use historiasclinicas\Http\Requests\StoryRequest;
use historiasclinicas\Http\Controllers\Controller;

use \historiasclinicas\Story;
use \historiasclinicas\Patient;

use Auth;

class StoryController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoryRequest $request)
    {
        if(Auth::user()->role=='admin'){
            $patient = Patient::findOrFail($request->patient_id);
        }else{
            $patient = Patient::where('id',$request->patient_id)
                              ->where('user_id', Auth::user()->id)
                              ->firstOrFail();
        }

        if(count($patient)>0){
            Story::create($request->all());
            return redirect('/patient/'.$request->patient_id)->with('status', 'Story Store!');
        }else{
            return redirect('/patient/'.$request->patient_id)->with('status', 'Story ERROR!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(Auth::user()->role=='admin'){
          $story = Story::findOrFail($id);
      }else{
          $patient = Story::find($id)
                        ->patient()
                        ->where('user_id', Auth::user()->id)
                        ->firstOrFail();
          $story = $patient->stories()->find($id);
      }
      return view('story.edit')->with(['story' => $story]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoryRequest $request, $id)
    {
      if(Auth::user()->role=='admin'){
          $story = Story::findOrFail($id);
      }else{
          $patient = Story::find($id)
                        ->patient()
                        ->where('user_id', Auth::user()->id)
                        ->firstOrFail();
          $story = $patient->stories()->find($id);
      }
      $story->fill($request->all());
      $story->save();
      return redirect('/patient/'.$story->patient_id)->with('status', 'Story Save!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $story = Story::findOrFail($id);
        $story->delete();
        return redirect('/patient/'.$story->patient_id)->with('status', 'Story Delete!');
    }
}
