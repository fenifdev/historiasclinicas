<?php

namespace historiasclinicas\Http\Controllers;

use Illuminate\Http\Request;

use historiasclinicas\Http\Requests;
use historiasclinicas\Http\Controllers\Controller;

use Excel;
use \historiasclinicas\User;

class ExcelController extends Controller
{
  public function create_users_report(){

    Excel::create('Users', function($excel) {
      // Set the title
      $excel->setTitle('Users');

      // Chain the setters
      $excel->setCreator('Maatwebsite')
            ->setCompany('Maatwebsite');

      // Call them separately
      $excel->setDescription('A demonstration to change the file properties');
      $excel->sheet('Usuarios', function($sheet) {
          $users = User::All();
          $sheet->fromArray($users);
      });
    })->export('xls');
  }
}
