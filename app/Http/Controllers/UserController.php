<?php

namespace historiasclinicas\Http\Controllers;

use Illuminate\Http\Request;

use historiasclinicas\Http\Requests;
use historiasclinicas\Http\Requests\UserRequest;
use historiasclinicas\Http\Controllers\Controller;

use \historiasclinicas\User;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Yajra\Datatables\Datatables;
use Form;

class UserController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->role=='admin'){
        $users = User::paginate(10);
        return view('user.index',compact('users'));
      }
      abort(404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role=='admin'){
          $data_role = ['doctor'=>'doctor','admin'=>'admin'];
          return view('user.create')->with(compact('data_role'));
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(Auth::user()->role=='admin'){
          User::create($request->all());
          return redirect('/user')->with('status', 'User Store!');
        }
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role=='admin'){
          $user = User::findOrFail($id);
          $data_role = ['doctor'=>'doctor','admin'=>'admin'];
        }else{
          $user = User::find(Auth::user()->id);
          $data_role = ['doctor'=>'doctor'];
        }
        return view('user.edit')->with(['user' => $user,'data_role' => $data_role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
      if(Auth::user()->role=='doctor'){
        $id = Auth::user()->id;
      }
      $user = User::findOrFail($id);
      $user->fill($request->all());
      $user->save();
      return redirect('/user')->with('status', 'User Save!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if(Auth::user()->role=='admin'){
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/user')->with('status', 'User Delete!');
      }
      abort(404);
    }

    public function dashboard()
    {
      return view('user.dashboard');
    }

    public function datatablesData()
    {
        #return Datatables::of(User::query())->make(true);
        $users = User::select(['id', 'name', 'email', 'created_at', 'updated_at']);
        return Datatables::of($users)->addColumn('action', function ($user) {
                $algo = link_to_route('user.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary']);
                $algo .= Form::open(['route' => ['user.destroy',$user->id] ,'method'=>'DELETE', 'style'=>'display:inline']);
                $algo .= Form::submit('Eliminar',['class'=>'btn btn-danger']);
                $algo .= Form::close();
                return $algo;
            })
            ->make(true);
    }
}
