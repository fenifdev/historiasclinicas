<?php

namespace historiasclinicas\Http\Controllers;

use Illuminate\Http\Request;

use historiasclinicas\Http\Requests;
use historiasclinicas\Http\Controllers\Controller;

use PDF;
use \historiasclinicas\User;

class PdfController extends Controller
{
  public function create_users_report(){
    $users = User::All();
    $pdf = PDF::loadView('pdf.users',['users'=>$users]);
    return $pdf->download('users.pdf');
  }
}
