<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UserController@dashboard');

Route::get('lang/{lang}',['middleware'=>'web', function ($lang) {
    session(['lang' => $lang]);
    return \Redirect::back();
  }])->where([
      'lang' => 'en|es'
  ]);

Route::get('pdf/create_users_report', 'PdfController@create_users_report');
Route::get('excel/create_users_report', 'ExcelController@create_users_report');

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('user/datatablesData', 'UserController@datatablesData');
Route::resource('user', 'UserController');
Route::get('patient/{id}/createStory', 'PatientController@createStory');
Route::get('patient/datatablesData', 'PatientController@datatablesData');
Route::resource('patient', 'PatientController');
Route::resource('story', 'StoryController');
