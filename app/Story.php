<?php

namespace historiasclinicas;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['story', 'patient_id'];

    public function patient()
    {
      return $this->belongsTo('historiasclinicas\Patient');
    }
}
