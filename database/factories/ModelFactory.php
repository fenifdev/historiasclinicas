<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(historiasclinicas\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => '123456',
        'remember_token' => str_random(10),
    ];
});

$factory->define(historiasclinicas\Patient::class, function (Faker\Generator $faker) {
    return [
        'dni' => $faker->randomNumber(9),
        'name' => $faker->name,
        'birthday' => $faker->dateTimeThisCentury->format('Y-m-d'),
    ];
});

$factory->define(historiasclinicas\Story::class, function (Faker\Generator $faker) {
    return [
        'story' => $faker->text(),
        'patient_id' => $faker->numberBetween(1,2500),
    ];
});
