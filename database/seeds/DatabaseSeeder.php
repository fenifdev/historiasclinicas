<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        factory(\historiasclinicas\User::class, 50)->create()->each(function($user){
          $user->patients()->saveMany(factory(\historiasclinicas\Patient::class, 50)->make());
        });
        factory(\historiasclinicas\Story::class, 50000)->create();
        Model::reguard();
    }
}
