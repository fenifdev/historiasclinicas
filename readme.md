## Instalation:

- Clone the repository.
- Set the folder with the right permissions.
- Run composer install
- Create .env file
- Run artisan key:generate
- Run php artisan migrate
- Run php artisan db:seed
