@extends('auth.template')
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::open(['url' => 'register', 'method'=>'POST'])!!}
    <h1>Create Account</h1>
    <div>
      {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre'])!!}
    </div>
    <div>
      {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
    </div>
    <div>
      {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
    </div>
    <div>
      {!!Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirmar Contraseña'])!!}
    </div>
    <div>
      {!!Form::submit('Registrar',['class'=>'btn btn-default submit'])!!}
    </div>

    <div class="clearfix"></div>

    <div class="separator">
      <p class="change_link">Already a member ?
        {!!link_to('auth/login', $title = ' Log in ', $attributes = ['class'=>'to_register'], $secure = null)!!}
      </p>
    </div>
  {!!Form::close()!!}
@stop
