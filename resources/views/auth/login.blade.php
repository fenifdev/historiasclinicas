@extends('auth.template')
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::open(['url' => 'login', 'method'=>'POST'])!!}
    <h1>Login Form</h1>
    <div>
      {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Email'])!!}
    </div>
    <div>
      {!!Form::password('password',['class'=>'form-control','placeholder'=>'Contraseña'])!!}
    </div>
    <div>
      {!!Form::submit('Login',['class'=>'btn btn-default submit'])!!}
      <a class="reset_pass" href="#">Lost your password?</a>
    </div>

    <div class="clearfix"></div>

    <div class="separator">
      <p class="change_link">New to site?
        {!!link_to('register', $title = ' Create Account ', $attributes = ['class'=>'to_register'], $secure = null)!!}
      </p>
    </div>
  {!!Form::close()!!}
@stop
