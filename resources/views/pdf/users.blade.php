<h1>Users</h1>
<table class='table'>
  <thead>
    <tr>
      <th>Nombre</th>
      <th>Correo</th>
    </tr>
  </thead>
  <tbody>
    @foreach($users as $user)
    <tr>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
