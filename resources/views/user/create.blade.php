@extends('layouts.admin')
@section('module_title')
  Usuarios
@endsection
@section('section_title')
  Crear Usuario
@endsection
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::open(['route' => 'user.store', 'method'=>'POST'])!!}
      <div class="form-group">
        {!!Form::label('Nombre')!!}
        {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingresa el nombre'])!!}
      </div>
      <div class="form-group">
        {!!Form::label('Email')!!}
        {!!Form::email('email',null,['class'=>'form-control','placeholder'=>'Ingresa el email'])!!}
      </div>
      <div class="form-group">
        {!!Form::label('Role')!!}
        {!!Form::select('role', $data_role, null, ['class'=>'form-control','placeholder' => 'Seleccione un doctor...'])!!}
      </div>
      <div class="form-group">
        {!!Form::label('Contraseña')!!}
        {!!Form::password('password',['class'=>'form-control','placeholder'=>'Ingresa el password'])!!}
      </div>
      {!!Form::submit('Crear',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}
@endsection
