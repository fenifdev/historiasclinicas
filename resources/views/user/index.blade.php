@extends('layouts.admin')
@section('module_title')
  Usuarios
@endsection
@section('section_title')
  Ver Usuarios
@endsection
@section('content')
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  <!--
  {!!link_to_action('PdfController@create_users_report', $title = 'PDF', $parameters = [], $attributes = ['class'=>'btn btn-danger'], $secure = null)!!}
  {!!link_to_action('ExcelController@create_users_report', $title = 'Excel', $parameters = [], $attributes = ['class'=>'btn btn-success'], $secure = null)!!}
  <table class='table'>
    <thead>
      <tr>
        <th>Nombre</th>
        <th>Correo</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $user)
      <tr>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>
          {!!link_to_route('user.edit', $title = 'Editar', $parameters = $user->id, $attributes = ['class'=>'btn btn-primary'])!!}
          {!!Form::open(['route' => ['user.destroy',$user->id] ,'method'=>'DELETE', 'style'=>'display:inline'])!!}
            {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
          {!!Form::close()!!}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {!!$users->render()!!}
-->
      <table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="users-table">
          <thead>
              <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <th>Action</th>
              </tr>
          </thead>
      </table>
@endsection
