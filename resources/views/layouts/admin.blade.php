<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Historias Clinicas! | </title>

    <!-- Bootstrap -->
    {!!Html::style('css/bootstrap.min.css')!!}
    <!-- Font Awesome -->
    {!!Html::style('css/font-awesome.min.css')!!}

    <!-- Custom Theme Style -->
    {!!Html::style('css/custom.min.css')!!}
    <!-- Datatables Theme Style -->
    {!!Html::style('css/dataTables/dataTables.bootstrap.min.css')!!}
    {!!Html::style('css/dataTables/buttons.bootstrap.min.css')!!}
    {!!Html::style('css/dataTables/responsive.bootstrap.min.css')!!}
    {!!Html::style('css/dataTables/responsive.bootstrap.min.css')!!}
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fa fa-paw"></i> <span>Historias Clinicas!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                <img src="{{url('/storage').'/'.(Auth::user()->avatar)}}" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>{!!Auth::user()->name!!}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Dashboard </a>
                  </li>
                  @if(Auth::user()->role=='admin')
                  <li><a><i class="fa fa-edit"></i> Usuarios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li>{!!link_to_action('UserController@index', $title = 'Ver', $attributes = [], $secure = null)!!}</li>
                      <li>{!!link_to_action('UserController@create', $title = 'Crear', $attributes = [], $secure = null)!!}</li>
                    </ul>
                  </li>
                  @endif
                  <li><a><i class="fa fa-desktop"></i> Pacientes <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li>{!!link_to_action('PatientController@index', $title = 'Ver', $attributes = [], $secure = null)!!}</li>
                      <li>{!!link_to_action('PatientController@create', $title = 'Crear', $attributes = [], $secure = null)!!}</li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="{{url('/storage').'/'.(Auth::user()->avatar)}}" alt="">{!!Auth::user()->name!!}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>@yield('module_title')</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>@yield('section_title')</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      @yield('content')
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <!-- jQuery -->
    {!!Html::script('js/jquery.min.js')!!}
    <!-- Bootstrap -->
    {!!Html::script('js/bootstrap.min.js')!!}
    <!-- Custom Theme Scripts -->
    {!!Html::script('js/custom.min.js')!!}
    <!-- DataTables -->
    <!--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>-->
    {!!Html::script('js/dataTables/jquery.dataTables.min.js')!!}
    {!!Html::script('js/dataTables/dataTables.responsive.min.js')!!}
    {!!Html::script('js/dataTables/dataTables.bootstrap.min.js')!!}
    {!!Html::script('js/dataTables/responsive.bootstrap.js')!!}
    <script>
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! url('user/datatablesData') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    $(function() {
        $('#patients-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! url('patient/datatablesData') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
    </script>
  </body>
</html>
