@extends('layouts.admin')
@section('module_title')
  Pacientes
@endsection
@section('section_title')
  Ver Paciente
@endsection
@section('content')
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  {!!link_to_action('PatientController@createStory', $title = 'Crear Historia', $parameters = $patient->id, $attributes = ['class'=>'btn btn-success'])!!}
  @if(count($patient->stories)==0)
    <div class="alert alert-danger text-center">
      No hay historias para este paciente.
    </div>
  @else
    <table class='table'>
      <thead>
        <tr>
          <th>Historia</th>
          <th>Fecha</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($patient->stories as $story)
        <tr>
          <td>{{$story->story}}</td>
          <td>{{$story->created_at->diffForHumans()}}</td>
          <td>
            {!!link_to_route('story.edit', $title = 'Editar', $parameters = $story->id, $attributes = ['class'=>'btn btn-primary'])!!}
            {!!Form::open(['route' => ['story.destroy',$story->id] ,'method'=>'DELETE', 'style'=>'display:inline'])!!}
              {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
            {!!Form::close()!!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  @endif
@endsection
