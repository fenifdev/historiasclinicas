@extends('layouts.admin')
@section('module_title')
  Pacientes
@endsection
@section('section_title')
  Ver Pacientes
@endsection
@section('content')
  @if (session('status'))
      <div class="alert alert-success">
          {{ session('status') }}
      </div>
  @endif
  @if(count($patients)==0)
    <div class="alert alert-danger text-center">
      No hay pacientes
    </div>
  @else
    <!--
    <table class='table'>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Dni</th>
          <th>Nacimiento</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach($patients as $patient)
        <tr>
          <td>{{$patient->name}}</td>
          <td>{{$patient->dni}}</td>
          <td>{{$patient->birthday}}</td>
          <td>
            {!!link_to_route('patient.edit', $title = 'Editar', $parameters = $patient->id, $attributes = ['class'=>'btn btn-primary'])!!}
            {!!link_to_route('patient.show', $title = 'Historia', $parameters = $patient->id, $attributes = ['class'=>'btn btn-info'])!!}
            {!!Form::open(['route' => ['patient.destroy',$patient->id] ,'method'=>'DELETE', 'style'=>'display:inline'])!!}
              {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
            {!!Form::close()!!}
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!!$patients->render()!!}
    -->
  @endif
  <table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="patients-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection
