@extends('layouts.admin')
@section('module_title')
  Pacientes
@endsection
@section('section_title')
  Crear Historia
@endsection
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::open(['route' => 'story.store', 'method'=>'POST'])!!}
    {!!Form::hidden('patient_id',$patient_id,null)!!}
      <div class="form-group">
        {!!Form::label('Historia')!!}
        {!!Form::textarea('story',null,['class'=>'form-control','placeholder'=>'ingresa la historia'])!!}
      </div>

      {!!Form::submit('Crear Paciente',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}
@endsection
