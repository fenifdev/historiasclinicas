@extends('layouts.admin')
@section('module_title')
  Pacientes
@endsection
@section('section_title')
  Crear Paciente
@endsection
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::open(['route' => 'patient.store', 'method'=>'POST'])!!}
      <div class="form-group">
        {!!Form::label('DNI')!!}
        {!!Form::text('dni',null,['class'=>'form-control','placeholder'=>'ingresa el dni'])!!}
      </div>
      <div class="form-group">
        {!!Form::label('Nombre')!!}
        {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'ingresa el nombre'])!!}
      </div>
      <div class="form-group">
        {!!Form::label('Nacimiento')!!}
        {!!Form::text('birthday',null,['class'=>'form-control','placeholder'=>'Fecha de nacimiento'])!!}
      </div>
      <div class="form-group">
        @if(Auth::user()->role=='admin')
        {!!Form::label('Doctor')!!}
        {!!Form::select('user_id', $data_users, null, ['class'=>'form-control','placeholder' => 'Seleccione un doctor...'])!!}
        @else
        {!!Form::select('user_id', $data_users, true, ['style'=>'display:none;'])!!}
        @endif
      </div>
      {!!Form::submit('Crear Paciente',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}
@endsection
