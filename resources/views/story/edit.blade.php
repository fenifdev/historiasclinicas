@extends('layouts.admin')
@section('module_title')
  Historias
@endsection
@section('section_title')
  Editar Historia
@endsection
@section('content')
  @if(count($errors)>0)
  <div class="alert alert-danger text-center">
    <ul>
      @foreach($errors->all() as $error)
      <li>{!!$error!!}</li>
      @endforeach
    </ul>
  </div>
  @endif
  {!!Form::model($story,['route' => ['story.update',$story->id], 'method'=>'PUT'])!!}
      <div class="form-group">
        {!!Form::label('Historia')!!}
        {!!Form::textarea('story',null,['class'=>'form-control','placeholder'=>'Ingresa la historia'])!!}
      </div>
      {!!Form::submit('Editar Historia',['class'=>'btn btn-primary'])!!}
  {!!Form::close()!!}
@endsection
